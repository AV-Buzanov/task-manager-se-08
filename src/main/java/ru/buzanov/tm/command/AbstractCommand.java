package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.enumerated.RoleType;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;
    protected BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull public abstract String command();

    @NotNull public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure() throws Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }

}
