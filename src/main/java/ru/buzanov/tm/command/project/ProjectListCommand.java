package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;

public class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println(serviceLocator.getProjectService().getList(userId));
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
