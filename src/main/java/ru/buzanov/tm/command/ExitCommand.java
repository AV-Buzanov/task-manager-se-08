package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;

public class ExitCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception {
        StringBuilder s = new StringBuilder("[GOODBYE! TILL WE MEET AGAIN!]");
        if (serviceLocator.getUserService().getCurrentUser() != null)
            s.insert(8, ", ").insert(10, serviceLocator.getUserService().getCurrentUser().getName());
        System.out.println(s);
        System.exit(0);
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
