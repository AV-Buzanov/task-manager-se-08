package ru.buzanov.tm.command;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Information about application";
    }

    @Override
    public void execute() throws Exception {

        System.out.println("Task-Manager ver.SE-07, " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
