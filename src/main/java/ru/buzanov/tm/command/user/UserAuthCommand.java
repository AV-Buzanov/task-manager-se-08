package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;

public class UserAuthCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "auth";
    }

    @NotNull
    @Override
    public String description() {
        return "User authentication";
    }

    @Override
    public void execute() throws Exception {
        String stringBuf;
        System.out.println("[AUTHORISATION]");
        System.out.println("[ENTER LOGIN]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            System.out.println("Login can't be empty");
            return;
        }
        if (!serviceLocator.getUserService().isLoginExist(stringBuf)) {
            System.out.println("User doesn't exist, register please");
            return;
        }
        User user = serviceLocator.getUserService().findByLogin(stringBuf);
        System.out.println("[ENTER PASS]");
        stringBuf = reader.readLine();
        if (!serviceLocator.getUserService().isPassCorrect(user.getLogin(), stringBuf)) {
            System.out.println("Invalid pass");
            return;
        }
        serviceLocator.getUserService().setCurrentUser(user);
        System.out.println("[HELLO, " + user.getName() + ", NICE TO SEE YOU!]");
    }

    @Override
    public boolean isSecure() throws Exception{
        return false;
    }
}
