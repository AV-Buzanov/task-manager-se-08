package ru.buzanov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.entity.Project;

@NoArgsConstructor
public class ProjectRepository extends AbstractSubjectRepository<Project> implements IProjectRepository {

    public void merge(@NotNull final String id, @NotNull final Project project) {
        if (project.getName() != null)
            map.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            map.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            map.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            map.get(id).setDescription(project.getDescription());
        if (project.getUserId() != null)
            map.get(id).setUserId(project.getUserId());
    }

    public void merge(@NotNull final String userId, @NotNull final String id, @NotNull final Project project) {
        if (!userId.equals(map.get(id).getUserId()))
            return;
        if (project.getName() != null)
            map.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            map.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            map.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            map.get(id).setDescription(project.getDescription());
        if (project.getUserId() != null)
            map.get(id).setUserId(project.getUserId());
    }
}