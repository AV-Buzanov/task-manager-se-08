package ru.buzanov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.Collection;

@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (User user : findAll()) {
            s.append(indexBuf).append(". ").append(user.getLogin());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(final int count) {
        int indexBuf = 1;
        for (User user : findAll()) {
            if (indexBuf == count)
                return user.getId();
            indexBuf++;
        }
        return null;
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        if (isLoginExist(login)) {
            for (User user : findAll()) {
                if (user.getLogin().equals(login))
                    return user;
            }
        }
        return null;
    }

    @NotNull
    public Collection<User> findByRole(@Nullable final RoleType role) {
        Collection<User> users = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getRoleType().equals(role))
                users.add(user);
        }
        return users;
    }

    public boolean isPassCorrect(@NotNull final String login, @NotNull final String pass) {
        User user = findByLogin(login);
        String s;
        if (user != null && user.getPasswordHash() != null) {
            s = user.getPasswordHash();
            return pass.equals(s);
        }
        return false;
    }

    public boolean isLoginExist(@NotNull final String login) {
        for (User user : findAll()) {
            if (user.getLogin().equals(login))
                return true;
        }
        return false;
    }

    public void merge(@NotNull final String id, @NotNull final User user) {
        if (user.getLogin() != null)
            this.map.get(id).setLogin(user.getLogin());
        if (user.getPasswordHash() != null)
            this.map.get(id).setPasswordHash(user.getPasswordHash());
        if (user.getRoleType() != null)
            this.map.get(id).setRoleType(user.getRoleType());
        if (user.getName() != null)
            this.map.get(id).setName(user.getName());
    }
}
