package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.service.ISubjectService;
import ru.buzanov.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends ISubjectService<Task> {

    @Nullable Collection<Task> findByProjectId(@Nullable String projectId);

    void removeByProjectId(@Nullable String projectId);

    @Nullable Collection<Task> findByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);
}
