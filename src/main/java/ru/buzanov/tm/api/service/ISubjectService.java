package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ISubjectService<T> extends IService<T>{

    @Nullable Collection<T> findAll(@Nullable String userId);

    @Nullable T findOne(@Nullable String userId, @Nullable String id);

    boolean isNameExist(String userId, String name);

    @NotNull String getList();

    @Nullable String getList(@Nullable String userId);

    @Nullable String getIdByCount(int count);

    @Nullable String getIdByCount(@Nullable String userId, int count);

    void merge(@Nullable String userId, @Nullable String id, @Nullable T project);

    @Nullable T remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);
}
