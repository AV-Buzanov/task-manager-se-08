package ru.buzanov.tm.api.service;

import ru.buzanov.tm.entity.Project;

import java.util.Collection;

public interface IProjectService extends ISubjectService<Project> {

}
