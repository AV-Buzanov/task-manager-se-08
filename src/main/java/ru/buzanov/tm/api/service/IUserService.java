package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserService extends IService<User> {

    @Nullable User findByLogin(@Nullable String login);

    boolean isPassCorrect(@Nullable String login, @Nullable String pass);

    boolean isLoginExist(@Nullable String login);

    @Nullable User getCurrentUser();

    void setCurrentUser(@Nullable User user);

    @Nullable Collection<User> findByRole(@Nullable RoleType role);
}
