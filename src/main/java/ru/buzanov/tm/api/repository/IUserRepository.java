package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @Nullable User findByLogin(@NotNull String login);

    boolean isPassCorrect(@NotNull String login, @NotNull String pass);

    boolean isLoginExist(@NotNull String login);

    @NotNull Collection<User> findByRole(@NotNull RoleType role);
}
