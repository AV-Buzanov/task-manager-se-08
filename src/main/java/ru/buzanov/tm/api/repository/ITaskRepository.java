package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ISubjectRepository;
import ru.buzanov.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends ISubjectRepository<Task> {

    @NotNull Collection<Task> findByProjectId(@NotNull String projectId);

    void removeByProjectId(@NotNull String projectId);

    @NotNull Collection<Task> findByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeByProjectId(@NotNull String userId, @NotNull String projectId);
}
