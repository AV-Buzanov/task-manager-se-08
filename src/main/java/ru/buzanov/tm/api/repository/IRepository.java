package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IRepository<T> {
    @NotNull Collection<T> findAll();

    @Nullable T findOne(@NotNull String id);

    void merge(@NotNull String id, @NotNull T entity);

    @Nullable T remove(@NotNull String id);

    void removeAll();

    @Nullable T load(@NotNull T entity);
}
