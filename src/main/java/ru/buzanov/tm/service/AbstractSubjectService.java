package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ISubjectRepository;
import ru.buzanov.tm.api.service.ISubjectService;
import ru.buzanov.tm.entity.AbstractSubjectEntity;

import java.util.Collection;

@NoArgsConstructor
public abstract class AbstractSubjectService<T extends AbstractSubjectEntity> extends AbstractService<T> implements ISubjectService<T> {
    private ISubjectRepository<T> abstractSubjectRepository;

    AbstractSubjectService(final ISubjectRepository<T> abstractRepository) {
        super(abstractRepository);
        this.abstractSubjectRepository = abstractRepository;
    }

    @Nullable
    public Collection<T> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.findAll(userId);
    }

    @Nullable
    public T findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.findOne(userId, id);
    }

    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty())
            return false;
        if (name == null || name.isEmpty())
            return false;
        return abstractSubjectRepository.isNameExist(userId, name);
    }

    @NotNull
    public String getList() {
        return abstractSubjectRepository.getList();
    }

    @Nullable
    public String getList(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getList(userId);
    }

    @Nullable
    public String getIdByCount(final int count) {
        return abstractSubjectRepository.getIdByCount(count);
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getIdByCount(userId, count);
    }

    public void merge(@Nullable final String userId, @Nullable final String id, @Nullable final T entity) {
        if (userId == null || userId.isEmpty())
            return;
        if (id == null || id.isEmpty())
            return;
        if (entity == null)
            return;
        abstractSubjectRepository.merge(userId, id, entity);
    }

    @Nullable
    public T remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.remove(userId, id);
    }

    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        abstractSubjectRepository.removeAll(userId);
    }
}
