# task-manager
## Software
+ Java 1.7
+ Maven 3.0.0
## Developer
+ name: Aleksey Buzanov
+ e-mail: av.buzanov@gmail.com
## Commands to build project
```
mvn clean
mvn install
```
## Commands to start application
```
java -jar task-manager.jar
```

